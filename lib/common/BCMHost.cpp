/*
 *      Copyright (C) 2005-2009 Team XBMC
 *      http://www.xbmc.org
 *
 *  This Program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This Program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with XBMC; see the file COPYING.  If not, write to
 *  the Free Software Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.
 *  http://www.gnu.org/copyleft/gpl.html
 *
 */

#include <bcm_host.h>
#include <iostream>
#include <raspimx/BCMHost.hpp>

using namespace raspimx;

BCMHost::BCMHost()
{
    m_initialized = false;
    Initialize();
}

BCMHost::~BCMHost()
{
    Deinitialize();
}

bool BCMHost::Initialize()
{
    std::cout << "bcm_host.init request\n";

    if (m_initialized)
        return true;

    bcm_host_init();
    std::cout << "bcm_host.init()\n";

    m_initialized = true;

    return true;
}

void BCMHost::Deinitialize()
{
    std::cout << "bcm_host.deinit request\n";
    if (!m_initialized)
        return;

    bcm_host_deinit();
    std::cout << "bcm_host.deinit()\n";
    m_initialized = false;
}

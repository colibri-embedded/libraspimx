#include <raspimx/Player.hpp>
#include <raspimx/PlayerImpl.hpp>

using namespace raspimx;

Player::Player()
{
    m_impl = new PlayerImpl();
}

Player::~Player()
{
    delete m_impl;
}

bool Player::SetWindowRectangle(unsigned x, unsigned y, unsigned width, unsigned height)
{
    return m_impl->SetWindowRectangle(x, y, width, height);
}

bool Player::SetWindowPosition(unsigned x, unsigned y)
{
    return m_impl->SetWindowPosition(x, y);
}

bool Player::SetWindowSize(unsigned width, unsigned height)
{
    return m_impl->SetWindowSize(width, height);
}

bool Player::SetWindowCropRectangle(unsigned x, unsigned y, unsigned width, unsigned height)
{
    return m_impl->SetWindowCropRectangle(x, y, width, height);
}

bool Player::SetWindowLayer(unsigned layer)
{
    return m_impl->SetWindowLayer(layer);
}

unsigned Player::GetWindowLayer()
{
    return m_impl->GetWindowLayer();
}

bool Player::SetWindowAlpha(unsigned alpha)
{
    return m_impl->SetWindowAlpha(alpha);
}

unsigned Player::GetWindowAlpha()
{
    return m_impl->GetWindowAlpha();
}

bool Player::SetWindowTransformation(unsigned transform)
{
    return m_impl->SetWindowTransformation(transform);
}

bool Player::OpenFile(const std::string& filename)
{
    return m_impl->OpenFile(filename);
}

bool Player::Play()
{
    return m_impl->Play();
}

bool Player::Stop()
{
    return m_impl->Stop();
}

bool Player::Pause()
{
    return m_impl->Pause();
}

bool Player::SetPosition(unsigned hours, unsigned minutes, unsigned seconds)
{
    return m_impl->SetPosition(hours, minutes, seconds);
}

bool Player::SetPosition(int position)
{
    return m_impl->SetPosition(position);
}

bool Player::SetVolume(float fVolume)
{
    return m_impl->SetVolume(fVolume);
}

float Player::GetVolume()
{
    return m_impl->GetVolume();
}

bool Player::SetMute(bool bOnOff)
{
    return m_impl->SetMute(bOnOff);
}

bool Player::GetMute()
{
    return m_impl->GetMute();
}
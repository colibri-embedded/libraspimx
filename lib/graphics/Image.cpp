#include <png.h>
#include <raspimx/Image.hpp>
#include <stdlib.h>

#ifndef ALIGN_TO_16
#define ALIGN_TO_16(x) ((x + 15) & ~15)
#endif

using namespace raspimx;

Image::Image()
  : Resource()
{}

Image::Image(const std::string& filename, DISPMANX_UPDATE_HANDLE_T update)
  : Resource()
{
    if (!LoadFromFile(filename, update))
        throw std::runtime_error("Cannot open requested file \"" + filename + "\"");
}

Image::~Image()
{
    if (m_data) {
        delete[] m_data;
        m_data = nullptr;
    }
}

uint8_t* Image::GetData() const
{
    return m_data;
}

void Image::InitImage(VC_IMAGE_TYPE_T type, unsigned width, unsigned height, bool dither)
{
    using namespace std::placeholders;

    switch (type) {
        case VC_IMAGE_4BPP:
            m_setPixelDirect  = nullptr;
            m_getPixelDirect  = nullptr;
            m_setPixelIndexed = std::bind(&Image::SetPixel4BPP, this, _1, _2, _3);
            m_getPixelIndexed = std::bind(&Image::GetPixel4BPP, this, _1, _2, _3);
            break;

        case VC_IMAGE_8BPP:
            m_setPixelDirect  = nullptr;
            m_getPixelDirect  = nullptr;
            m_setPixelIndexed = std::bind(&Image::SetPixel8BPP, this, _1, _2, _3);
            m_getPixelIndexed = std::bind(&Image::GetPixel8BPP, this, _1, _2, _3);
            break;

        case VC_IMAGE_RGB565:
            if (dither) {
                m_setPixelDirect = std::bind(&Image::SetPixelDitheredRGB565, this, _1, _2, _3);
            } else {
                m_setPixelDirect = std::bind(&Image::SetPixelRGB565, this, _1, _2, _3);
            }
            m_getPixelDirect  = std::bind(&Image::GetPixelRGB565, this, _1, _2, _3);
            m_setPixelIndexed = nullptr;
            m_getPixelIndexed = nullptr;
            break;

        case VC_IMAGE_RGB888:
            m_setPixelDirect  = std::bind(&Image::SetPixelRGB888, this, _1, _2, _3);
            m_getPixelDirect  = std::bind(&Image::GetPixelRGB888, this, _1, _2, _3);
            m_setPixelIndexed = nullptr;
            m_getPixelIndexed = nullptr;
            break;

        case VC_IMAGE_RGBA16:
            if (dither) {
                m_setPixelDirect = std::bind(&Image::SetPixelDitheredRGBA16, this, _1, _2, _3);
            } else {
                m_setPixelDirect = std::bind(&Image::SetPixelRGBA16, this, _1, _2, _3);
            }
            m_getPixelDirect  = std::bind(&Image::GetPixelRGBA16, this, _1, _2, _3);
            m_setPixelIndexed = nullptr;
            m_getPixelIndexed = nullptr;
            break;

        case VC_IMAGE_RGBA32:
            m_setPixelDirect  = std::bind(&Image::SetPixelRGBA32, this, _1, _2, _3);
            m_getPixelDirect  = std::bind(&Image::GetPixelRGBA32, this, _1, _2, _3);
            m_setPixelIndexed = nullptr;
            m_getPixelIndexed = nullptr;
            break;

        default:;
            // fprintf(stderr, "image: unknown type (%d)\n", type);
            throw std::runtime_error("Unknown image type");
    }

    if (!Create(width, height, type))
        throw std::runtime_error("Cannot allocate vc resource");

    m_size = m_pitch * m_aligned_height;

    // cleanup previous data
    if (m_data)
        delete[] m_data;

    m_data = new uint8_t[m_size];

    if (m_data == nullptr)
        throw std::runtime_error("Not enough memory to allocate image");
}

bool Image::LoadFromFile(const std::string& filename, DISPMANX_UPDATE_HANDLE_T update)
{
    FILE* file = fopen(filename.c_str(), "rb");

    if (file == nullptr)
        return false;

    png_structp png_ptr = png_create_read_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);

    if (png_ptr == NULL) {
        return false;
    }

    png_infop info_ptr = png_create_info_struct(png_ptr);

    if (info_ptr == NULL) {
        png_destroy_read_struct(&png_ptr, 0, 0);
        return false;
    }

    if (setjmp(png_jmpbuf(png_ptr))) {
        png_destroy_read_struct(&png_ptr, &info_ptr, 0);
        return false;
    }

    //---------------------------------------------------------------------

    png_init_io(png_ptr, file);

    png_read_info(png_ptr, info_ptr);

    //---------------------------------------------------------------------

    png_byte colour_type = png_get_color_type(png_ptr, info_ptr);
    png_byte bit_depth   = png_get_bit_depth(png_ptr, info_ptr);

    VC_IMAGE_TYPE_T type = VC_IMAGE_RGB888;

    if (colour_type & PNG_COLOR_MASK_ALPHA) {
        type = VC_IMAGE_RGBA32;
    }

    InitImage(type,
              png_get_image_width(png_ptr, info_ptr),
              png_get_image_height(png_ptr, info_ptr),
              false);

    //---------------------------------------------------------------------

    double gamma = 0.0;

    if (png_get_gAMA(png_ptr, info_ptr, &gamma)) {
        png_set_gamma(png_ptr, 2.2, gamma);
    }

    //---------------------------------------------------------------------

    if (colour_type == PNG_COLOR_TYPE_PALETTE) {
        png_set_palette_to_rgb(png_ptr);
    }

    if ((colour_type == PNG_COLOR_TYPE_GRAY) && (bit_depth < 8)) {
        png_set_expand_gray_1_2_4_to_8(png_ptr);
    }

    if (png_get_valid(png_ptr, info_ptr, PNG_INFO_tRNS)) {
        png_set_tRNS_to_alpha(png_ptr);
    }

    if (bit_depth == 16) {
#ifdef PNG_READ_SCALE_16_TO_8_SUPPORTED
        png_set_scale_16(png_ptr);
#else
        png_set_strip_16(png_ptr);
#endif
    }

    if (colour_type == PNG_COLOR_TYPE_GRAY || colour_type == PNG_COLOR_TYPE_GRAY_ALPHA) {
        png_set_gray_to_rgb(png_ptr);
    }

    //---------------------------------------------------------------------

    png_read_update_info(png_ptr, info_ptr);

    //---------------------------------------------------------------------

    png_bytepp row_pointers = (png_bytepp)malloc(m_rect.height * sizeof(png_bytep));

    png_uint_32 j = 0;
    for (j = 0; j < (png_uint_32)m_rect.height; ++j) {
        row_pointers[j] = m_data + (j * m_pitch);
    }

    //---------------------------------------------------------------------

    png_read_image(png_ptr, row_pointers);

    //---------------------------------------------------------------------

    free(row_pointers);

    png_destroy_read_struct(&png_ptr, &info_ptr, 0);

    fclose(file);

    return WriteData(m_data);
}

bool Image::SetPixelIndexed(int32_t x, int32_t y, int8_t index)
{
    bool result = false;

    if ((m_setPixelIndexed != NULL) && (x >= 0) && (x < m_rect.width) && (y >= 0) &&
        (y < m_rect.height)) {
        result = true;
        m_setPixelIndexed(x, y, index);
    }

    return result;
}

//-------------------------------------------------------------------------

bool Image::SetPixelRGB(int32_t x, int32_t y, const RGBA8_T* rgb)
{
    bool result = false;

    if ((m_setPixelDirect != nullptr) && (x >= 0) && (x < m_rect.width) && (y >= 0) &&
        (y < m_rect.height)) {
        result = true;
        m_setPixelDirect(x, y, rgb);
    }

    return result;
}

//-------------------------------------------------------------------------

bool Image::GetPixelIndexed(int32_t x, int32_t y, int8_t* index)
{
    bool result = false;

    if ((m_getPixelIndexed != NULL) && (x >= 0) && (x < m_rect.width) && (y >= 0) &&
        (y < m_rect.height)) {
        result = true;
        m_getPixelIndexed(x, y, index);
    }

    return result;
}

//-------------------------------------------------------------------------

bool Image::GetPixelRGB(int32_t x, int32_t y, RGBA8_T* rgb)
{
    bool result = false;

    if ((m_getPixelDirect != NULL) && (x >= 0) && (x < m_rect.width) && (y >= 0) &&
        (y < m_rect.height)) {
        result = true;
        m_getPixelDirect(x, y, rgb);
    }

    return result;
}

void Image::SetPixel4BPP(int32_t x, int32_t y, int8_t index)
{
    index &= 0x0F;

    uint8_t* value = (uint8_t*)(m_data + (x / 2) + (y * m_pitch));

    if (x % 2) {
        *value = (*value & 0xF0) | (index);
    } else {
        *value = (*value & 0x0F) | (index << 4);
    }
}

//-----------------------------------------------------------------------

void Image::SetPixel8BPP(int32_t x, int32_t y, int8_t index)
{
    *(uint8_t*)(m_data + x + (y * m_pitch)) = index;
}

//-----------------------------------------------------------------------

void Image::SetPixelRGB565(int32_t x, int32_t y, const RGBA8_T* rgba)
{
    uint8_t r5 = rgba->red >> 3;
    uint8_t g6 = rgba->green >> 2;
    uint8_t b5 = rgba->blue >> 3;

    uint16_t pixel                                 = (r5 << 11) | (g6 << 5) | b5;
    *(uint16_t*)(m_data + (x * 2) + (y * m_pitch)) = pixel;
}

//-----------------------------------------------------------------------

void Image::SetPixelDitheredRGB565(int32_t x, int32_t y, const RGBA8_T* rgba)
{
    static int16_t dither8[64] = {
        1, 6, 2, 7, 1, 6, 2, 7, 4, 2, 5, 4, 4, 3, 6, 4, 1, 7, 1, 6, 2, 7,
        1, 7, 5, 3, 5, 3, 5, 4, 5, 3, 1, 6, 2, 7, 1, 6, 2, 7, 4, 3, 6, 4,
        4, 2, 6, 4, 2, 7, 1, 7, 2, 7, 1, 6, 5, 3, 5, 3, 5, 3, 5, 3,
    };

    static int16_t dither4[64] = {
        1, 3, 1, 3, 1, 3, 1, 3, 2, 1, 3, 2, 2, 1, 3, 2, 1, 3, 1, 3, 1, 3,
        1, 3, 2, 2, 2, 1, 3, 2, 2, 2, 1, 3, 1, 3, 1, 3, 1, 3, 2, 1, 3, 2,
        2, 1, 3, 2, 1, 3, 1, 3, 1, 3, 1, 3, 3, 2, 2, 2, 2, 2, 2, 2,
    };

    int32_t index = (x & 7) | ((y & 7) << 3);

    int16_t r = rgba->red + dither8[index];

    if (r > 255) {
        r = 255;
    }

    int16_t g = rgba->green + dither4[index];

    if (g > 255) {
        g = 255;
    }

    int16_t b = rgba->blue + dither8[index];

    if (b > 255) {
        b = 255;
    }

    RGBA8_T dithered = { (uint8_t)r, (uint8_t)g, (uint8_t)b, rgba->alpha };

    SetPixelRGB565(x, y, &dithered);
}

//-------------------------------------------------------------------------

void Image::SetPixelRGB888(int32_t x, int32_t y, const RGBA8_T* rgba)
{
    uint8_t* line = (uint8_t*)(m_data) + (y * m_pitch) + (3 * x);
    line[0]       = rgba->red;
    line[1]       = rgba->green;
    line[2]       = rgba->blue;
}

//-----------------------------------------------------------------------

void Image::SetPixelRGBA16(int32_t x, int32_t y, const RGBA8_T* rgba)
{
    uint8_t r4 = rgba->red >> 4;
    uint8_t g4 = rgba->green >> 4;
    uint8_t b4 = rgba->blue >> 4;
    uint8_t a4 = rgba->alpha >> 4;

    uint16_t pixel                                 = (r4 << 12) | (g4 << 8) | (b4 << 4) | a4;
    *(uint16_t*)(m_data + (x * 2) + (y * m_pitch)) = pixel;
}

//-----------------------------------------------------------------------

void Image::SetPixelDitheredRGBA16(int32_t x, int32_t y, const RGBA8_T* rgba)
{
    static int16_t dither16[64] = {
        1, 12, 4,  15, 1, 13, 4,  15, 8,  4,  11, 7,  9,  5,  12, 8,  3,  14, 2, 13, 3,  15,
        2, 14, 10, 6,  9, 5,  11, 7,  10, 6,  1,  12, 4,  15, 1,  12, 4,  15, 9, 5,  12, 8,
        8, 5,  11, 8,  3, 14, 2,  13, 3,  14, 2,  13, 11, 7,  10, 6,  10, 7,  9, 6,
    };

    int32_t index = (x & 7) | ((y & 7) << 3);

    int16_t r = rgba->red + dither16[index];

    if (r > 255) {
        r = 255;
    }

    int16_t g = rgba->green + dither16[index];

    if (g > 255) {
        g = 255;
    }

    int16_t b = rgba->blue + dither16[index];

    if (b > 255) {
        b = 255;
    }

    int16_t a = rgba->alpha + dither16[index];

    if (b > 255) {
        b = 255;
    }

    RGBA8_T dithered = { (uint8_t)r, (uint8_t)g, (uint8_t)b, (uint8_t)a };

    SetPixelRGBA16(x, y, &dithered);
}

//-----------------------------------------------------------------------

void Image::SetPixelRGBA32(int32_t x, int32_t y, const RGBA8_T* rgba)
{
    uint8_t* line = (uint8_t*)(m_data) + (y * m_pitch) + (4 * x);

    line[0] = rgba->red;
    line[1] = rgba->green;
    line[2] = rgba->blue;
    line[3] = rgba->alpha;
}

//-----------------------------------------------------------------------

void Image::GetPixel4BPP(int32_t x, int32_t y, int8_t* index)
{
    uint8_t* value = (uint8_t*)(m_data + (x / 2) + (y * m_pitch));

    if (x % 2) {
        *index = (*value) & 0x0F;
    } else {
        *index = (*value) >> 4;
    }
}

//-----------------------------------------------------------------------

void Image::GetPixel8BPP(int32_t x, int32_t y, int8_t* index)
{
    *index = *(uint8_t*)(m_data + x + (y * m_pitch));
}

//-----------------------------------------------------------------------

void Image::GetPixelRGB565(int32_t x, int32_t y, RGBA8_T* rgba)
{
    uint16_t pixel = *(uint16_t*)(m_data + (x * 2) + (y * m_pitch));

    uint8_t r5 = (pixel >> 11) & 0x1F;
    uint8_t g6 = (pixel >> 5) & 0x3F;
    uint8_t b5 = pixel & 0x1F;

    rgba->red   = (r5 << 3) | (r5 >> 2);
    rgba->green = (g6 << 2) | (g6 >> 4);
    rgba->blue  = (b5 << 3) | (b5 >> 2);
    rgba->alpha = 255;
}

//-------------------------------------------------------------------------

void Image::GetPixelRGB888(int32_t x, int32_t y, RGBA8_T* rgba)
{
    uint8_t* line = (uint8_t*)(m_data) + (y * m_pitch) + (3 * x);
    rgba->red     = line[0];
    rgba->green   = line[1];
    rgba->blue    = line[2];
    rgba->alpha   = 255;
}

//-----------------------------------------------------------------------

void Image::GetPixelRGBA16(int32_t x, int32_t y, RGBA8_T* rgba)
{
    uint16_t pixel = *(uint16_t*)(m_data + (x * 2) + (y * m_pitch));
    uint8_t r4     = (pixel >> 12) & 0xF;
    uint8_t g4     = (pixel >> 8) & 0xF;
    uint8_t b4     = (pixel >> 4) & 0xF;
    uint8_t a4     = (pixel & 0xF);

    rgba->red   = (r4 << 4) | r4;
    rgba->green = (g4 << 4) | g4;
    rgba->blue  = (b4 << 4) | b4;
    rgba->alpha = (a4 << 4) | a4;
}

//-----------------------------------------------------------------------

void Image::GetPixelRGBA32(int32_t x, int32_t y, RGBA8_T* rgba)
{
    uint8_t* line = (uint8_t*)(m_data) + (y * m_pitch) + (4 * x);

    rgba->red   = line[0];
    rgba->green = line[1];
    rgba->blue  = line[2];
    rgba->alpha = line[3];
}
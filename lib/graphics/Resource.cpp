#include <iostream>
#include <raspimx/Resource.hpp>
#include <stdexcept>

#ifndef ALIGN_TO_16
#define ALIGN_TO_16(x) ((x + 15) & ~15)
#endif

using namespace raspimx;

Resource::Resource() {}

Resource::Resource(int32_t width, int32_t height, VC_IMAGE_TYPE_T type)
{
    Create(width, height, type);
}

Resource::~Resource()
{
    Destroy();
}

bool Resource::WriteData(uint8_t* data)
{
    if (!m_handle)
        return false;

    auto result = vc_dispmanx_resource_write_data(m_handle, m_type, m_pitch, data, &(m_rect));
    std::cout << "Resource.WriteData " << result << "\n";
    return (result == 0);
}

bool Resource::ReadData(uint8_t* data)
{
    if (!m_handle)
        return false;

    auto result = vc_dispmanx_resource_read_data(m_handle, (&m_rect), data, m_pitch);
    return (result == 0);
}

DISPMANX_RESOURCE_HANDLE_T Resource::GetHandle() const
{
    return m_handle;
}

bool Resource::Create(int32_t width, int32_t height, VC_IMAGE_TYPE_T type)
{
    if (m_handle)
        Destroy();

    m_rect = { 0, 0, width, height };
    m_type = type;

    std::cout << "Resource.Create " << width << ", " << height << ", " << type << "\n";

    switch (type) {
        case VC_IMAGE_4BPP:
            m_bits_per_pixel = 4;
            break;

        case VC_IMAGE_8BPP:
            m_bits_per_pixel = 8;
            break;

        case VC_IMAGE_RGB565:
            m_bits_per_pixel = 16;
            break;

        case VC_IMAGE_RGB888:
            m_bits_per_pixel = 24;
            break;

        case VC_IMAGE_RGBA16:
            m_bits_per_pixel = 16;
            break;

        case VC_IMAGE_RGBA32:
            m_bits_per_pixel = 32;
            break;

        default:
            // fprintf(stderr, "image: unknown type (%d)\n", type);
            // throw std::runtime_error("Unknown image type");
            return false;
    }

    m_pitch          = (ALIGN_TO_16(width) * m_bits_per_pixel) / 8;
    m_aligned_height = ALIGN_TO_16(height);

    uint32_t vc_image_ptr;
    m_handle = vc_dispmanx_resource_create(m_type,
                                           m_rect.width | (m_pitch << 16),
                                           m_rect.height | (m_aligned_height << 16),
                                           &vc_image_ptr);

    TriggerEvent(Event::HANDLER_CHANGED);

    return (m_handle != 0);
}

void Resource::Destroy()
{
    if (m_handle)
        vc_dispmanx_resource_delete(m_handle);
    m_handle = 0;
    TriggerEvent(Event::HANDLER_DELETED);
}

VC_IMAGE_TYPE_T Resource::GetType() const
{
    return m_type;
}

int32_t Resource::GetWidth() const
{
    return m_rect.width;
}

int32_t Resource::GetHeight() const
{
    return m_rect.height;
}

int32_t Resource::GetPitch() const
{
    return m_pitch;
}

int32_t Resource::GetAlignedHeight() const
{
    return m_aligned_height;
}

uint16_t Resource::GetBitsPerPixel() const
{
    return m_bits_per_pixel;
}

void Resource::AddEventListener(IEventListener* listener)
{
    m_event_listeners.push_back(listener);
}

void Resource::RemoveEventListener(IEventListener*) {}

void Resource::TriggerEvent(Event event)
{
    for (auto listener : m_event_listeners) {
        listener->HandleEvent(event, this);
    }
}
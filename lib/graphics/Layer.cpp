#include <iostream>
#include <raspimx/Layer.hpp>

using namespace raspimx;

#ifndef ELEMENT_CHANGE_LAYER
#define ELEMENT_CHANGE_LAYER (1 << 0)
#endif

#ifndef ELEMENT_CHANGE_OPACITY
#define ELEMENT_CHANGE_OPACITY (1 << 1)
#endif

#ifndef ELEMENT_CHANGE_DEST_RECT
#define ELEMENT_CHANGE_DEST_RECT (1 << 2)
#endif

#ifndef ELEMENT_CHANGE_SRC_RECT
#define ELEMENT_CHANGE_SRC_RECT (1 << 3)
#endif

#ifndef ELEMENT_CHANGE_MASK_RESOURCE
#define ELEMENT_CHANGE_MASK_RESOURCE (1 << 4)
#endif

#ifndef ELEMENT_CHANGE_TRANSFORM
#define ELEMENT_CHANGE_TRANSFORM (1 << 5)
#endif

Layer::Layer(int32_t display)
  : m_display(display)
{}

bool Layer::SetLayerDestinationRectangle(unsigned x, unsigned y, unsigned width, unsigned height)
{
    m_dst_rect.x      = x;
    m_dst_rect.y      = y;
    m_dst_rect.width  = width;
    m_dst_rect.height = height;
    ChangeAttribute(ELEMENT_CHANGE_DEST_RECT);
    return true;
}

bool Layer::SetLayerPosition(unsigned x, unsigned y)
{
    m_dst_rect.x = x;
    m_dst_rect.y = y;
    ChangeAttribute(ELEMENT_CHANGE_DEST_RECT);
    return true;
}

bool Layer::SetLayerSize(unsigned width, unsigned height)
{
    m_dst_rect.width  = width;
    m_dst_rect.height = height;
    ChangeAttribute(ELEMENT_CHANGE_DEST_RECT);
    return true;
}

bool Layer::SetLayerSourceRectangle(unsigned x, unsigned y, unsigned width, unsigned height)
{
    m_src_rect.x      = x << 16;
    m_src_rect.y      = y << 16;
    m_src_rect.width  = width << 16;
    m_src_rect.height = height << 16;
    ChangeAttribute(ELEMENT_CHANGE_SRC_RECT);
    return true;
}

bool Layer::SetLayerIndex(unsigned layer)
{
    m_layer = layer;
    ChangeAttribute(ELEMENT_CHANGE_LAYER);
    return true;
}

unsigned Layer::GetIndexLayer()
{
    return m_layer;
}

bool Layer::SetLayerAlpha(unsigned alpha)
{
    m_alpha = alpha;
    ChangeAttribute(ELEMENT_CHANGE_OPACITY);
    return true;
}

unsigned Layer::GetLayerAlpha()
{
    return m_alpha;
}

bool Layer::SetLayerTransformation(unsigned transform)
{
    m_transform = transform;
    ChangeAttribute(ELEMENT_CHANGE_TRANSFORM);
    return true;
}

void Layer::ChangeAttribute(uint32_t change_flags)
{
    if (!m_handle)
        return;

    DISPMANX_UPDATE_HANDLE_T update = vc_dispmanx_update_start(0);
    if (update == 0) {
        std::cout << "Cannot change attribute\n";
        return;
    }

    auto result = vc_dispmanx_element_change_attributes(update,
                                                        m_handle,
                                                        change_flags,
                                                        m_layer,
                                                        m_alpha,
                                                        &(m_dst_rect),
                                                        &(m_src_rect),
                                                        0,
                                                        (DISPMANX_TRANSFORM_T)m_transform);

    if (result != 0) {
        std::cout << "Cannot change attribute\n";
        return;
    }

    // if (change_flags & ELEMENT_CHANGE_OPACITY)
    // vc_d

    vc_dispmanx_update_submit_sync(update);
}

void Layer::SetResource(Resource* res)
{
    // TODO: detach previouse resource
    if (m_resource != nullptr) {
        m_resource->RemoveEventListener(this);
    }

    m_resource = res;
    m_resource->AddEventListener(this);
    SetLayerSourceRectangle(0, 0, m_resource->GetWidth(), m_resource->GetHeight());
}

Resource* Layer::GetResource() const
{
    return m_resource;
}

bool Layer::Show()
{
    if (m_resource == nullptr)
        return false;

    // std::cout << "Layer.Show\n";

    DISPMANX_DISPLAY_HANDLE_T display = vc_dispmanx_display_open(m_display);
    // assert(display != 0);

    // DISPMANX_MODEINFO_T info;
    // int result = vc_dispmanx_display_get_info(display, &info);
    // assert(result == 0);

    DISPMANX_UPDATE_HANDLE_T update = vc_dispmanx_update_start(0);
    if (update == 0)
        return false;

    VC_DISPMANX_ALPHA_T alpha = { DISPMANX_FLAGS_ALPHA_FIXED_ALL_PIXELS,
                                  m_alpha, /*alpha 0->255*/
                                  0 };

    m_handle = vc_dispmanx_element_add(update,
                                       display,
                                       m_layer,
                                       &(m_dst_rect),
                                       m_resource->GetHandle(),
                                       &(m_src_rect),
                                       DISPMANX_PROTECTION_NONE,
                                       &alpha,
                                       NULL, // clamp
                                       DISPMANX_NO_ROTATE);

    auto result = vc_dispmanx_update_submit_sync(update);

    return (result == 0);
}

bool Layer::Hide()
{
    if (!m_handle)
        return false;

    DISPMANX_UPDATE_HANDLE_T update = vc_dispmanx_update_start(0);
    vc_dispmanx_element_remove(update, m_handle);
    auto result = vc_dispmanx_update_submit_sync(update);
    return (result == 0);
}

void Layer::HandleEvent(Event event, void* userdata)
{
    auto res = (Resource*)userdata;

    if (res->GetHandle()) {
        DISPMANX_UPDATE_HANDLE_T update = vc_dispmanx_update_start(0);
        vc_dispmanx_element_change_source(update, m_handle, res->GetHandle());
        auto result = vc_dispmanx_update_submit_sync(update);
        if (result != 0) {
            Hide();
        }
    }
}
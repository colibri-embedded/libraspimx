#ifndef RASPIMX_PLAYER_IMPL_H
#define RASPIMX_PLAYER_IMPL_H

#include <getopt.h>
#include <signal.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <termios.h>

#define AV_NOWARN_DEPRECATED

extern "C"
{
#include <libavformat/avformat.h>
#include <libavutil/avutil.h>
};

#include "OMXStreamInfo.h"
#include "revision.h"

#include "utils/log.h"

#include "DllAvCodec.h"
#include "DllAvFormat.h"
#include "DllAvUtil.h"
#include "linux/RBP.h"

#include "DllOMX.h"
#include "OMXAudio.h"
#include "OMXAudioCodecOMX.h"
#include "OMXClock.h"
#include "OMXPlayerAudio.h"
#include "OMXPlayerVideo.h"
#include "OMXReader.h"
#include "OMXThread.h"
#include "OMXVideo.h"
#include "Srt.h"
#include "utils/PCMRemap.h"
#include "utils/Strprintf.h"

#include <raspimx/BCMHost.hpp>
#include <raspimx/interface/IPlayer.hpp>
#include <raspimx/interface/IWindow.hpp>
#include <utils/ThreadSafeQueue.hpp>

#include <memory>
#include <string>
#include <utility>

namespace raspimx {

    enum FORMAT_3D_T
    {
        CONF_FLAGS_FORMAT_NONE,
        CONF_FLAGS_FORMAT_SBS,
        CONF_FLAGS_FORMAT_TB,
        CONF_FLAGS_FORMAT_FP
    };

    enum class PlayerAction
    {
        NONE = 0,
        OPEN_FILE,
        DECREASE_SPEED,
        INCREASE_SPEED,
        REWIND,
        FORWARD,
        STEP,
        PREVIOUS_AUDIO,
        NEXT_AUDIO,
        // PREVIOUS_CHAPTER,
        // NEXT_CHAPTER,
        PREVIOUS_SUBTITLE,
        NEXT_SUBTITLE,
        TOGGLE_SUBTITLE,
        HIDE_SUBTITLES,
        SHOW_SUBTITLES,
        DECREASE_SUBTITLE_DELAY,
        INCREASE_SUBTITLE_DELAY,

        SEEK_BACK,    // amount, relative|absolute
        SEEK_FORWARD, // amount, relative|absolute

        SET_POSITION,

        SET_ALPHA,
        SET_LAYER,

        PLAY,
        STOP,
        PAUSE,
        RESUME,

        MOVE_VIDEO,
        CROP_VIDEO,

        HIDE_VIDEO,
        UNHIDE_VIDEO,

        SET_ASPECT_MODE,

        DECREASE_VOLUME,
        INCREASE_VOLUME
    };

    enum class PlayerState
    {
        IDLE = 0,
        READY,
        PLAYING,
        PAUSED,
        FINISHED
    };

    struct PlayerCommand
    {
        PlayerAction action  = PlayerAction::NONE;
        int iarg[4]          = { 0, 0, 0, 0 };
        std::string filename = "";
    };

    class PlayerImpl
      : public OMXThread
      , public IPlayer
      , public IWindow
    {
      public:
        PlayerImpl();
        virtual ~PlayerImpl();

        bool OpenFile(const std::string& filename);

        bool Play() override;
        bool Stop() override;
        bool Pause() override;
        bool SetPosition(int pos) override;
        bool SetPosition(unsigned hours, unsigned minutes, unsigned seconds) override;

        bool SetVolume(float fVolume) override;
        float GetVolume() override;
        bool SetMute(bool bOnOff) override;
        bool GetMute() override;

        bool SetWindowRectangle(unsigned x, unsigned y, unsigned width, unsigned height) override;
        bool SetWindowPosition(unsigned x, unsigned y) override;
        bool SetWindowSize(unsigned width, unsigned height) override;
        bool SetWindowCropRectangle(unsigned x,
                                    unsigned y,
                                    unsigned width,
                                    unsigned height) override;
        bool SetWindowLayer(unsigned layer) override;
        unsigned GetWindowLayer() override;
        bool SetWindowAlpha(unsigned alpha) override;
        unsigned GetWindowAlpha() override;
        bool SetWindowTransformation(unsigned transform) override;

      protected:
        BCMHost& m_bmc_host = BCMHost::GetInstance();

        thread_safe::Queue<PlayerCommand> m_command_queue;
        PlayerState m_player_state = PlayerState::IDLE;

        std::string m_filename = "";

        enum PCMChannels* m_pChannelMap = NULL;
        long m_Volume                   = 0;
        long m_Amplification            = 0;
        bool m_NativeDeinterlace        = false;
        // bool m_osd = !is_model_pi4() && !is_fkms_active();

        bool m_centered  = false;
        bool m_ghost_box = true;

        int m_audio_index_use     = 0;
        bool m_no_hdmi_clock_sync = false;
        bool m_stop               = false;
        int m_subtitle_index      = -1;

        TV_DISPLAY_STATE_T tv_state;
        bool m_refresh = false;
        // int m_tv_show_info = 0;
        bool m_has_video = false;
        bool m_has_audio = false;

        bool m_loop = false;

        // CRBP g_RBP;
        COMXCore g_OMX;
        OMXReader m_omx_reader;
        OMXVideoConfig m_config_video;
        OMXPlayerVideo m_player_video;
        OMXAudioConfig m_config_audio;
        OMXPlayerAudio m_player_audio;
        OMXClock* m_av_clock = NULL;
        OMXPacket* m_omx_pkt = NULL;
        DllBcmHost m_BcmHost;

        void Process(); // Main loop

        bool LoadFile(const std::string& filename);
        bool IsURL(const std::string& str);
        bool Exists(const std::string& path);
        bool IsPipe(const std::string& str);

        void Cleanup();
        void FlushStreams(double pts);

        int GetGPUMemory(void);
        float GetDisplayAspectRatio(SDTV_ASPECT_T aspect);
        float GetDisplayAspectRatio(HDMI_ASPECT_T aspect);

        void SetSpeed(int iSpeed);
        void SetVideoMode(int width, int height, int fpsrate, int fpsscale, FORMAT_3D_T is3d);

        void SetBackground(uint32_t rgba);
    };

} // namespace raspimx

#endif /* RASPIMX_PLAYER_IMPL_H */
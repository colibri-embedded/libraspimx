#ifndef RASPIMX_BCM_HOST_HPP
#define RASPIMX_BCM_HOST_HPP

#include <memory>

namespace raspimx {

    class BCMHost
    {
      public:
        ~BCMHost();

        bool Initialize();
        void Deinitialize();

        static BCMHost& GetInstance()
        {
            static BCMHost INSTANCE;
            return INSTANCE;
        }

      private:
        BCMHost();

        bool m_initialized;
    };

} // namespace raspimx

#endif /* RASPIMX_BCM_HOST_HPP */

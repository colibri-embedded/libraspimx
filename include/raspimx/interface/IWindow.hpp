#ifndef RASPIMX_IWINDOW_HPP
#define RASPIMX_IWINDOW_HPP

#include <bcm_host.h>

namespace raspimx {

    enum Transform
    {
        NO_ROTATE  = 0,
        ROTATE_90  = 1,
        ROTATE_180 = 2,
        ROTATE_270 = 3,

        FLIP_HRIZ = 1 << 16,
        FLIP_VERT = 1 << 17,
    };

    class IWindow
    {
      public:
        virtual bool SetWindowRectangle(unsigned x,
                                        unsigned y,
                                        unsigned width,
                                        unsigned height) = 0;

        virtual bool SetWindowPosition(unsigned x, unsigned y) = 0;

        virtual bool SetWindowSize(unsigned width, unsigned height) = 0;

        virtual bool SetWindowCropRectangle(unsigned x,
                                            unsigned y,
                                            unsigned width,
                                            unsigned height) = 0;

        virtual bool SetWindowLayer(unsigned layer) = 0;
        virtual unsigned GetWindowLayer()           = 0;

        virtual bool SetWindowAlpha(unsigned alpha) = 0;
        virtual unsigned GetWindowAlpha()           = 0;

        virtual bool SetWindowTransformation(unsigned transform) = 0;
    };

} // namespace raspimx

#endif /* RASPIMX_IWINDOW_HPP */
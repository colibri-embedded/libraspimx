#ifndef IPLAYER_H
#define IPLAYER_H

#include <string>

namespace raspimx {

    class IPlayer
    {
      public:
        /**
         * Open a video or audio file
         *
         * @param filename
         * @return true on success, false otherwise
         **/
        virtual bool OpenFile(const std::string& filename) = 0;

        /**
         * Start media file playback
         *
         * @return true on success, false otherwise
         **/
        virtual bool Play() = 0;

        /**
         * Stop playback and seek to file beginning
         *
         * @return true on success, false otherwise
         **/
        virtual bool Stop() = 0;

        /**
         * Pause playback
         *
         * @return true on success, false otherwise
         **/
        virtual bool Pause() = 0;

        /**
         * Seek to specific position
         *
         * @return true on success, false otherwise
         **/
        virtual bool SetPosition(int position) = 0;

        virtual bool SetPosition(unsigned hours, unsigned minutes, unsigned seconds) = 0;

        virtual bool SetVolume(float fVolume) = 0;

        virtual float GetVolume() = 0;

        virtual bool SetMute(bool bOnOff) = 0;

        virtual bool GetMute() = 0;
    };

} // namespace raspimx

#endif /* IPLAYER_H */
#ifndef RASPIMX_IEVENT_LISTENER
#define RASPIMX_IEVENT_LISTENER

#include <bcm_host.h>
#include <functional>

namespace raspimx {

    enum Event
    {
        NO_EVENT = 0,
        HANDLER_CHANGED,
        HANDLER_DELETED,
        SOURCE_CHANGED
    };

    class IEventListener
    {
      public:
        virtual void HandleEvent(Event, void*) = 0;
    };

} // namespace raspimx

#endif /* RASPIMX_IEVENT_LISTENER */
#ifndef RASPIMX_ILAYER_HPP
#define RASPIMX_ILAYER_HPP

#include <bcm_host.h>

namespace raspimx {

    enum Transform
    {
        NO_ROTATE  = 0,
        ROTATE_90  = 1,
        ROTATE_180 = 2,
        ROTATE_270 = 3,

        FLIP_HRIZ = 1 << 16,
        FLIP_VERT = 1 << 17,
    };

    class ILayer
    {
      public:
        virtual bool SetLayerDestinationRectangle(unsigned x,
                                                  unsigned y,
                                                  unsigned width,
                                                  unsigned height) = 0;

        virtual bool SetLayerPosition(unsigned x, unsigned y) = 0;

        virtual bool SetLayerSize(unsigned width, unsigned height) = 0;

        virtual bool SetLayerSourceRectangle(unsigned x,
                                             unsigned y,
                                             unsigned width,
                                             unsigned height) = 0;

        virtual bool SetLayerIndex(unsigned layer) = 0;
        virtual unsigned GetIndexLayer()           = 0;

        virtual bool SetLayerAlpha(unsigned alpha) = 0;
        virtual unsigned GetLayerAlpha()           = 0;

        virtual bool SetLayerTransformation(unsigned transform) = 0;
    };

} // namespace raspimx

#endif /* RASPIMX_ILAYER_HPP */
#ifndef RASPIMX_LAYER_HPP
#define RASPIMX_LAYER_HPP

#include <raspimx/BCMHost.hpp>
#include <raspimx/Resource.hpp>
#include <raspimx/interface/ILayer.hpp>

namespace raspimx {

    class Layer
      : public ILayer
      , public IEventListener
    {
      public:
        Layer(int32_t display = 0);
        // Layer(int32_t x, int32_t y, int32_t width, int32_t height, int32_t layer_index = 1);
        bool SetLayerDestinationRectangle(unsigned x,
                                          unsigned y,
                                          unsigned width,
                                          unsigned height) override;

        bool SetLayerPosition(unsigned x, unsigned y) override;

        bool SetLayerSize(unsigned width, unsigned height) override;

        bool SetLayerSourceRectangle(unsigned x,
                                     unsigned y,
                                     unsigned width,
                                     unsigned height) override;

        bool SetLayerIndex(unsigned layer) override;
        unsigned GetIndexLayer() override;

        bool SetLayerAlpha(unsigned alpha) override;
        unsigned GetLayerAlpha() override;

        bool SetLayerTransformation(unsigned transform) override;

        bool Show();
        bool Hide();

        void SetResource(Resource* res);
        Resource* GetResource() const;

        void HandleEvent(Event, void*) override;

      protected:
        BCMHost& m_bmc_host = BCMHost::GetInstance();

        VC_RECT_T m_src_rect               = { 0, 0, 0, 0 };
        VC_RECT_T m_dst_rect               = { 0, 0, 0, 0 };
        unsigned m_alpha                   = 255;
        int32_t m_layer                    = 1;
        int32_t m_display                  = 0;
        unsigned m_transform               = DISPMANX_NO_ROTATE;
        DISPMANX_ELEMENT_HANDLE_T m_handle = 0;

        Resource* m_resource = nullptr;

        void ChangeAttribute(uint32_t change_flags);
    };

} // namespace raspimx

#endif /* RASPIMX_LAYER_HPP */
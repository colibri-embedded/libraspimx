#ifndef DISPMANX_RESOURCE_HPP
#define DISPMANX_RESOURCE_HPP

#include <bcm_host.h>
#include <cstdint>
#include <functional>
#include <raspimx/BCMHost.hpp>
#include <raspimx/interface/IEventListener.hpp>
#include <vector>

namespace raspimx {

    class Resource
    {
      public:
        Resource();
        Resource(int32_t width, int32_t height, VC_IMAGE_TYPE_T type = VC_IMAGE_ARGB8888);
        ~Resource();

        bool Create(int32_t width, int32_t height, VC_IMAGE_TYPE_T type);

        bool WriteData(uint8_t* data);
        bool ReadData(uint8_t* data);

        DISPMANX_RESOURCE_HANDLE_T GetHandle() const;

        VC_IMAGE_TYPE_T GetType() const;
        int32_t GetWidth() const;
        int32_t GetHeight() const;
        int32_t GetPitch() const;
        int32_t GetAlignedHeight() const;
        uint16_t GetBitsPerPixel() const;

        void AddEventListener(IEventListener*);
        void RemoveEventListener(IEventListener*);
        void TriggerEvent(Event);

      protected:
        BCMHost& m_bmc_host = BCMHost::GetInstance();

        VC_RECT_T m_rect                    = { 0, 0, 0, 0 };
        VC_IMAGE_TYPE_T m_type              = VC_IMAGE_ARGB8888;
        DISPMANX_RESOURCE_HANDLE_T m_handle = 0;

        int32_t m_pitch           = 0;
        int32_t m_aligned_height  = 0;
        uint16_t m_bits_per_pixel = 0;

        std::vector<IEventListener*> m_event_listeners;

        void Destroy();
    };

} // namespace raspimx

#endif /* DISPMANX_RESOURCE_HPP */
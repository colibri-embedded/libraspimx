#ifndef RASPIMX_IMAGE_HPP
#define RASPIMX_IMAGE_HPP

#include <bcm_host.h>
#include <cstdint>
#include <functional>
#include <memory>
#include <raspimx/Resource.hpp>
#include <string>

namespace raspimx {

    struct RGBA8_T
    {
        uint8_t red;
        uint8_t green;
        uint8_t blue;
        uint8_t alpha;
    };

    class Image : public Resource
    {
      public:
        Image();
        Image(const std::string& filename, DISPMANX_UPDATE_HANDLE_T update = 0);
        ~Image();

        bool LoadFromFile(const std::string& filename, DISPMANX_UPDATE_HANDLE_T update = 0);

        bool SetPixelRGB(int32_t x, int32_t y, const RGBA8_T*);
        bool GetPixelRGB(int32_t x, int32_t y, RGBA8_T* rgba);
        bool SetPixelIndexed(int32_t x, int32_t y, int8_t index);
        bool GetPixelIndexed(int32_t x, int32_t y, int8_t* index);

        uint8_t* GetData() const;

      private:
        uint8_t* m_data = nullptr;
        uint32_t m_size = 0;

        std::function<void(int32_t, int32_t, const RGBA8_T*)> m_setPixelDirect;
        std::function<void(int32_t, int32_t, RGBA8_T*)> m_getPixelDirect;
        std::function<void(int32_t, int32_t, int8_t)> m_setPixelIndexed;
        std::function<void(int32_t, int32_t, int8_t*)> m_getPixelIndexed;

        void InitImage(VC_IMAGE_TYPE_T type, unsigned width, unsigned height, bool dither);

        void SetPixel4BPP(int32_t x, int32_t y, int8_t index);
        void SetPixel8BPP(int32_t x, int32_t y, int8_t index);
        void SetPixelRGB565(int32_t x, int32_t y, const RGBA8_T* rgba);
        void SetPixelDitheredRGB565(int32_t x, int32_t y, const RGBA8_T* rgba);
        void SetPixelRGB888(int32_t x, int32_t y, const RGBA8_T* rgba);
        void SetPixelRGBA16(int32_t x, int32_t y, const RGBA8_T* rgba);
        void SetPixelDitheredRGBA16(int32_t x, int32_t y, const RGBA8_T* rgba);
        void SetPixelRGBA32(int32_t x, int32_t y, const RGBA8_T* rgba);

        void GetPixel4BPP(int32_t x, int32_t y, int8_t* index);
        void GetPixel8BPP(int32_t x, int32_t y, int8_t* index);
        void GetPixelRGB565(int32_t x, int32_t y, RGBA8_T* rgba);
        void GetPixelRGB888(int32_t x, int32_t y, RGBA8_T* rgba);
        void GetPixelRGBA16(int32_t x, int32_t y, RGBA8_T* rgba);
        void GetPixelRGBA32(int32_t x, int32_t y, RGBA8_T* rgba);
    };

} // namespace raspimx

#endif /* RASPIMX_IMAGE_HPP */
#ifndef RASPIMX_PLAYER_H
#define RASPIMX_PLAYER_H

#include <raspimx/interface/IPlayer.hpp>
#include <raspimx/interface/IWindow.hpp>
#include <string>

namespace raspimx {

    class PlayerImpl;

    class Player
      : public IPlayer
      , public IWindow
    {
      public:
        Player();
        ~Player();

        /**
         * Open a video or audio file
         *
         * @param filename
         * @return true on success, false otherwise
         **/
        bool OpenFile(const std::string& filename) override;

        /**
         * Start media file playback
         *
         * @return true on success, false otherwise
         **/
        bool Play() override;

        /**
         * Stop playback and seek to file beginning
         *
         * @return true on success, false otherwise
         **/
        bool Stop() override;

        /**
         * Pause playback
         *
         * @return true on success, false otherwise
         **/
        bool Pause() override;

        /**
         * Seek to specific position
         *
         * @return true on success, false otherwise
         **/
        bool SetPosition(int position) override;

        bool SetPosition(unsigned hours, unsigned minutes, unsigned seconds) override;

        bool SetVolume(float fVolume) override;

        float GetVolume() override;

        bool SetMute(bool bOnOff) override;

        bool GetMute() override;

        /**
         * Set window rectangle
         *
         * @param x Window top left x coordinate in pixels
         * @param y Window top left y coordinate in pixels
         * @param width Window width in pixels
         * @param height Window height in pixels
         **/
        bool SetWindowRectangle(unsigned x, unsigned y, unsigned width, unsigned height) override;

        bool SetWindowPosition(unsigned x, unsigned y) override;

        bool SetWindowSize(unsigned width, unsigned height) override;

        bool SetWindowCropRectangle(unsigned x,
                                    unsigned y,
                                    unsigned width,
                                    unsigned height) override;
        bool SetWindowLayer(unsigned layer) override;
        unsigned GetWindowLayer() override;

        bool SetWindowAlpha(unsigned alpha) override;
        unsigned GetWindowAlpha() override;

        bool SetWindowTransformation(unsigned transform) override;

      private:
        PlayerImpl* m_impl;
    };

} // namespace raspimx

#endif /* RASPIMX_PLAYER_H */
#include <chrono>
#include <iostream>
#include <signal.h>
#include <thread>

#include <raspimx/Player.hpp>

volatile sig_atomic_t g_abort = false;

void sig_handler(int s)
{
    if (s == SIGINT && !g_abort) {
        signal(SIGINT, SIG_DFL);
        g_abort = true;
        return;
    }
    signal(SIGABRT, SIG_DFL);
    signal(SIGSEGV, SIG_DFL);
    signal(SIGFPE, SIG_DFL);

    //   abort();
}

int main()
{
    signal(SIGSEGV, sig_handler);
    signal(SIGABRT, sig_handler);
    signal(SIGFPE, sig_handler);
    signal(SIGINT, sig_handler);

    raspimx::Player player1, player2, player3, player4;

    player1.SetWindowRectangle(120, 80, 480, 320);
    player1.SetWindowLayer(1);

    player1.SetVolume(1000);
    player1.OpenFile("/root/Kristina-jablcko.mkv");
    player1.SetPosition(0, 0, 10);
    // player2.SetWindowRect(120,240,240,160);
    // player2.SetLayer(2);
    // player2.OpenFile("/root/nokka.mp3");

    // player3.SetWindowRect(240,200,240,160);
    // player3.SetAlpha(128);
    // player3.SetLayer(3);
    // player3.OpenFile("/root/melody.mp3");

    // player4.SetWindowRect(240,80,240,160);
    // player4.SetAlpha(200);
    // player4.SetLayer(4);
    // player4.SetMute(true);
    // player4.OpenFile("/mnt/userdata/resources/video/intro_video.mp4");

    player1.Play();
    // player4.Play();
    // player2.Play();
    // player3.Play();

    std::this_thread::sleep_for(std::chrono::seconds(5));
    // player1.SetWindowSize(240, 160);
    // player1.Stop();
    // player1.OpenFile("/mnt/userdata/resources/video/intro_video.mp4");
    // player1.Play();
    // player4.SetAlpha(128);
    // player4.SetVolume(0.25);
    // std::this_thread::sleep_for(std::chrono::seconds(5));
    // player4.SetVolume(0.1);
    // std::this_thread::sleep_for(std::chrono::seconds(1));
    // player4.SetVolume(0.0);
    // player.SetWindowRect(120,80,360,240);
    // player.OpenFile("/root/small.mp4");
    // // std::this_thread::sleep_for(std::chrono::seconds(2));
    // player.Play();
    // std::cout << "Request: PLAY\n";
    // std::this_thread::sleep_for(std::chrono::seconds(2));
    // // player.Pause();
    // // std::cout << "Request: PAUSE\n";
    // // std::this_thread::sleep_for(std::chrono::seconds(2));
    // // player.Play();
    // // std::cout << "Request: PLAY\n";
    // // std::this_thread::sleep_for(std::chrono::seconds(5));
    // player4.Stop();
    // std::cout << "Request: STOP\n";
    // std::this_thread::sleep_for(std::chrono::seconds(2));

    // player.SetWindowRect(120,240,360,400);
    // player.OpenFile("/mnt/userdata/resources/video/intro_video.mp4");
    // player.Play();
    // // player.Play();
    // // std::cout << "Request: PLAY\n";
    // // player.SeekTo(0);
    // // std::cout << "Request: SEEK-TO\n";
    // // player.Play();

    while (true) {
        if (g_abort) {
            break;
        }
        std::this_thread::sleep_for(std::chrono::milliseconds(100));
    }

    return 0;
}
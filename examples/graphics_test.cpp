#include <chrono>
#include <iostream>
#include <raspimx/Image.hpp>
#include <raspimx/Layer.hpp>
#include <thread>

using namespace raspimx;

volatile sig_atomic_t g_abort = false;

void sig_handler(int s)
{
    if (s == SIGINT && !g_abort) {
        signal(SIGINT, SIG_DFL);
        g_abort = true;
        return;
    }
    signal(SIGABRT, SIG_DFL);
    signal(SIGSEGV, SIG_DFL);
    signal(SIGFPE, SIG_DFL);

    abort();
}

int main()
{
    signal(SIGSEGV, sig_handler);
    signal(SIGABRT, sig_handler);
    signal(SIGFPE, sig_handler);
    signal(SIGINT, sig_handler);
    // Image im1;
    // if (im1.loadFromPNG("480x320.png")) {
    //     std::cout << "Success!\n";
    // }

    // Resource res(480, 320);
    Image im1("480x320_color.png");
    Image im2("transparent.png");
    Layer l1, l2;
    l1.SetLayerDestinationRectangle(120, 80, 240, 160);
    l1.SetResource(&im1);
    // l1.SetLayerAlpha(128);
    l1.SetLayerIndex(2);

    l2.SetLayerDestinationRectangle(160, 120, 240, 160);
    l2.SetResource(&im2);
    // l2.SetLayerAlpha(128);
    l2.SetLayerIndex(3);

    l1.Show();
    l2.Show();

    std::this_thread::sleep_for(std::chrono::seconds(2));

    im1.LoadFromFile("480x320.png");
    // l1.Hide();
    l1.SetLayerAlpha(64);
    l2.SetLayerAlpha(64);
    l2.SetLayerIndex(1);

    std::this_thread::sleep_for(std::chrono::seconds(2));

    // l1.Show();
    l1.SetLayerTransformation(Transform::ROTATE_180);

    std::this_thread::sleep_for(std::chrono::seconds(1));

    // ImageWindow iw;
    // iw.GetImage().LoadFromPNG("480x320.png");
    // iw.Show();

    // ImageWindow iw2;
    // iw2.GetImage().LoadFromPNG("480x320.png");
    // iw2.Show();

    // while (true) {
    //     if (g_abort) {
    //         break;
    //     }
    //     std::this_thread::sleep_for(std::chrono::milliseconds(100));
    // }

    std::this_thread::sleep_for(std::chrono::seconds(3));

    return 0;
}